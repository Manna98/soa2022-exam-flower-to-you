import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
        ClientsModule.register([
            { name: 'CATALOG_MICROSERVICE', transport: Transport.TCP , options: { port: 4000}},
            { name: 'POST_MICROSERVICE', transport: Transport.TCP, options: { port: 4001} },
        ])
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
