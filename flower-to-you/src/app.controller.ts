import { Controller, Get, Param, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

    @Get()
    getHello(): string {
      return this.appService.getHello();
    }

    @Get('/delivery/:id')
    getDeliveryById(@Param('id') id: number) {
        return this.appService.getDeliveryById(id);
    }

    @Post('/delivery')
    createDelivery(@Body() createDeliveryDto) {
        return this.appService.createDelivery(createDeliveryDto);
    }

    @Post('/findFlower')
    getFlowerByCaracteristics(@Body() orderDto) {
        return this.appService.getFlowerByCaracteristics(orderDto);
    }

    @Post('/flower')
    createFlower(@Body() createFlowerDto) {
        return this.appService.createFlower(createFlowerDto);
    }

    @Get('/flower')
    getAllFlowers() {
        return this.appService.getAllFlowers();
    }   

    @Get('/catalog')
    initCatalog() {
        return this.appService.initCatalog();
    }
}
