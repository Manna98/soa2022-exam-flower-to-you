import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class AppService {
    constructor(
        @Inject('POST_MICROSERVICE') private readonly clientDelivery: ClientProxy,
        @Inject('CATALOG_MICROSERVICE') private readonly clientCatalog: ClientProxy
    ) { }

    getHello(): string {
    return 'Hello World!';
    }

    getDeliveryById(id: number) {
        return this.clientDelivery.send({ role: 'delivery', cmd: 'get-by-id' }, id);
    }

    createDelivery(createDeliveryDto) {
        return this.clientDelivery.send({ role: 'delivery', cmd: 'create' }, createDeliveryDto);
    }

    getAllFlowers() {
        return this.clientCatalog.send({ role: 'catalog', cmd: 'get-all' },"");
    }

    getFlowerByCaracteristics(orderDto) {
        return this.clientCatalog.send({ role: 'catalog', cmd: 'get-best-match' }, orderDto);
    }

    createFlower(createFlowerDto) {
        return this.clientCatalog.send({ role: 'catalog', cmd: 'create' }, createFlowerDto);
    }

    initCatalog() {
        return this.clientCatalog.send({ role: 'catalog', cmd: 'init' }, "");
    }
}
