import { EntityRepository, Repository } from "typeorm";
import { FlowerEntity } from "./flower.entity";
@EntityRepository(FlowerEntity)
export class FlowerRepository extends Repository<FlowerEntity> { }