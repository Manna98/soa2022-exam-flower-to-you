import { Injectable } from '@nestjs/common';
import { FlowerEntity } from './flower.entity';
import { FlowerRepository } from './flower.repository';
import { Between } from "typeorm";

@Injectable()
export class AppService {
    constructor(
        private readonly flowerRepository: FlowerRepository,
    ) { }

    getHello(): string {
        return 'Hello World!';
    }

    initCatalog() {
        const f1 = this.flowerRepository.create({ name: "rose", size:"M", color: "red", fragnant: false, price: 12, potted: false, blooming: true, pictureUrl: "https://sc04.alicdn.com/kf/UTB8jh1IJiaMiuJk43PTq6ySmXXan.jpg" });
        const f2 = this.flowerRepository.create({ name: "lily", size: "M",color: "white", fragnant: true, price: 8, potted: false, blooming: true, pictureUrl: "https://www.funnyhowflowersdothat.co.uk/sites/flowers/files/styles/portrait_full/public/lelie_mooiwatbloemendoen_rouwboeket_4.jpg?itok=BLuJnQyK" });
        const f3 = this.flowerRepository.create({ name: "hydrangea", size: "L", color: "blue", fragnant: false, price: 30, potted: true, blooming: true, pictureUrl: "https://img.teleflora.com/images/o_0/l_flowers:T89-2A,pg_6/w_800,h_1000,cs_no_cmyk,c_pad/f_jpg,q_auto:eco,e_sharpen:200/flowers/T89-2A/HappyHydrangea-Blue" });
        const f4 = this.flowerRepository.create({ name: "orchid", size: "L", color: "yellow", fragnant: false, price: 20, potted: true, blooming: true, pictureUrl: "https://www.floravietnam.com/images/detailed/2/3-yellow-orchid-stems.jpg" });
        const f5 = this.flowerRepository.create({ name: "tulip", size: "M", color: "red", fragnant: false, price: 8, potted: false, blooming: true, pictureUrl: "https://jooinn.com/images/red-tulip-6.jpg" });
        const f6 = this.flowerRepository.create({ name: "cactus", size: "S", color: "green", fragnant: false, price: 5, potted: true, blooming: false, pictureUrl: "https://s13emagst.akamaized.net/products/32299/32298016/images/res_ee2d74cccdd71c5df3d0ab6456be97bf.jpg" });
        this.flowerRepository.save(f1);
        this.flowerRepository.save(f2);
        this.flowerRepository.save(f3);
        this.flowerRepository.save(f4);
        this.flowerRepository.save(f5);
        this.flowerRepository.save(f6);
        return "catalog is initialized";
    }

    createFlower(flowerDto) {
        const flower = new FlowerEntity();
        flower.name = flowerDto.name;
        flower.size = flowerDto.size;
        flower.color = flowerDto.color;
        flower.fragnant = flowerDto.fragnant;
        flower.price = flowerDto.price;
        flower.potted = flowerDto.potted;
        flower.blooming = flowerDto.blooming;
        flower.pictureUrl = flowerDto.pictureUrl;
        return this.flowerRepository.save(flower);
    }

    getAllFlowers() {
        return this.flowerRepository.find();
    }

    getFlowerById(id) {
        return this.flowerRepository.findOne(id);
    }

    findBestMatch(orderDto) {
        //the flower that matches the most arguments

        const matches = {};
        const findAndIncrement = function (array) {
            array.map(entity => { matches[entity.id] ? matches[entity.id]++ : (matches[entity.id] = 1) });
        };
        const failedf = function () { return "db exception" };

        return this.flowerRepository.find({ where: { color: orderDto.color } })
            .then(fs1 => {
                findAndIncrement(fs1);
                return this.flowerRepository.find({ where: { fragnant: orderDto.fragnant } });
            }, () => failedf()+"1")
            .then(fs2 => {
                findAndIncrement(fs2);
                return this.flowerRepository.find({ where: { potted: orderDto.potted } });
            }, () => failedf() + "2")
            .then(fs3 => {
                findAndIncrement(fs3);
                return this.flowerRepository.find({ where: { blooming: orderDto.blooming } });
            }, () => failedf() + "3")
            .then(fs4 => {
                findAndIncrement(fs4);
                return this.flowerRepository.find({ where: { price: Between(orderDto.minPrice, orderDto.maxPrice) } });
            }, () => failedf() + "4")
            .then(fs5 => {
                findAndIncrement(fs5);
                const flowerID = Object.keys(matches).reduce((a, b) => matches[a] > matches[b] ? a : b);
                if (matches[flowerID] == 0) {
                    return null;
                } else {
                    return this.flowerRepository.findOne(flowerID);
                }
            }, () => failedf() + "5");
    }
}
