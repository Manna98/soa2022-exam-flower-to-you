import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
@Entity()
export class FlowerEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    name: string;
    @Column()
    potted: boolean;
    @Column()
    color: string;
    @Column()
    fragnant: boolean;
    @Column()
    price: number;
    @Column()
    blooming: boolean;
    @Column()
    size: string;
    @Column()
    pictureUrl: string;
}