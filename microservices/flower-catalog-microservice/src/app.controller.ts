import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { MessagePattern } from '@nestjs/microservices';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) { }

    @MessagePattern({ cmd: 'hello' })
    hello(input?: string): string {
        return `Hello, ${input || 'there'}!`;
    }

    @MessagePattern({ role: 'catalog', cmd: 'create' })
    createFlower(flowerDto) {
        return this.appService.createFlower(flowerDto);
    }

    @MessagePattern({ role: 'catalog', cmd: 'get-all' })
    getAllFlowers() {
        return this.appService.getAllFlowers();
    }

    @MessagePattern({ role: 'catalog', cmd: 'get-by-id' })
    getFlowerById(id: number) {
        return this.appService.getFlowerById(id);
    }

    @MessagePattern({ role: 'catalog', cmd: 'get-best-match' })
    findBestMatch(orderDto) {
        return this.appService.findBestMatch(orderDto);
    }
    
    @MessagePattern({ role: 'catalog', cmd: 'init' })
    initCatalog() {
        return this.appService.initCatalog();
    }
}
