import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FlowerEntity } from './flower.entity';
import { FlowerRepository } from './flower.repository';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'postgres',
            host: 'localhost',
            port: 5433,
            username: 'smis2358',
            password: 'psw',
            database: 'catalog_db',
            synchronize: true,
            autoLoadEntities: true,
        }),
        TypeOrmModule.forFeature([FlowerRepository, FlowerEntity])
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule { }