import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
@Entity()
export class DeliveryEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    userId: string;
    @Column()
    flowerId: number;
    @Column()
    address: string;
}