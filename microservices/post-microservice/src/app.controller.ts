import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { MessagePattern } from '@nestjs/microservices';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

    @MessagePattern({ cmd: 'hello' })
    hello(input?: string): string {
        return `Hello, ${input || 'there'}!`;
    }
    
    @MessagePattern({ role: 'delivery', cmd: 'create' })
    createDelivery(deliveryDto) {
        return this.appService.createDelivery(deliveryDto);
    }

    @MessagePattern({ role: 'delivery', cmd: 'get-by-id' })
    getDeliveryById(id: number) {
        return this.appService.getDeliveryById(id);
    }
}
