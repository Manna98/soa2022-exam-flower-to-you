import { EntityRepository, Repository } from "typeorm";
import { DeliveryEntity } from "./delivery.entity";
@EntityRepository(DeliveryEntity)
export class DeliveryRepository extends Repository<DeliveryEntity> { }