import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DeliveryEntity } from './delivery.entity';
import { DeliveryRepository } from './delivery.repository';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'postgres',
            host: 'localhost',
            port: 5435,
            username: 'smis2358',
            password: 'psw',
            database: 'post_db',
            synchronize: true,
            autoLoadEntities: true,
        }),
        TypeOrmModule.forFeature([DeliveryRepository, DeliveryEntity])
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule { }