import { Injectable } from '@nestjs/common';
import { DeliveryEntity } from './delivery.entity';
import { DeliveryRepository } from './delivery.repository';

@Injectable()
export class AppService {
    constructor(
        private readonly deliveryRepository: DeliveryRepository,
    ) { }

    getHello(): string {
        return 'Hello World!';
    }

    createDelivery(deliveryDto) {
        const delivery = new DeliveryEntity();
        delivery.userId = deliveryDto.userId;
        delivery.flowerId = deliveryDto.flowerId;
        delivery.address = deliveryDto.address;
        
        return this.deliveryRepository.save(delivery);
    }

    getDeliveryById(id) {
        return this.deliveryRepository.findOne(id);
    }

}
